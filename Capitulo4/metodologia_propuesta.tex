\chapter{Metodología para el desarrollo de aplicaciones IoT, MeDAIC}



\noindent Como resultado de la preocupación por el creciente número de aplicaciones de la IoT surge
la presente metodología de desarrollo (Figura \ref{metodologia}), MeDAIC, que ha sido ideada para dirigirse a ingenieros de software
que comienzan a incursionar en el desarrollo de aplicaciones de la IoT y a aquellos con experiencia \textit{ad hoc} en el área; por esta razón MeDAIC
hace uso de modelos intencionales para la temprana captura de requisitos, que posteriormente como parte del proceso,
serán refinados para continuar con una concisa metodología de desarrollo de software.\\

\noindent Las partes que comprenden este enfoque de ingeniería de software son:

\begin{enumerate}
    \item Definición del propósito y requisitos de alto nivel.
    \item Refinamiento de los requisitos de alto nivel.
    \item Especificación del modelo de información de entidades físicas y virtuales.
    \item Definición del formato y estructura para el intercambio de datos.
    \item Visión general de la arquitectura.
    \item Construcción, pruebas y validación.
    \item Desarrollo del diagrama de despliegue.
    \item Implantación del sistema.
\end{enumerate}


\begin{figure}[h]
  \centering
    \includegraphics[scale=0.2875]{Capitulo4/figs/met.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Metodología para el desarrollo de aplicaciones IoT}            %Pie de imagen
  \label{metodologia}                            %nombre de referencia
\end{figure}



\section{Definición del propósito y requisitos de alto nivel}

\noindent El primer paso de MeDAIC consiste en la definición del propósito del sistema mediante una reunión con los stakeholders y el establecimiento de los requisitos de alto nivel
por medio de un modelo estratégico de dependencias i*, SDM por sus siglas en inglés. \\

SDM es un modelo que se forma a partir de un ``conjunto de nodos
y ligas, donde cada nodo representa a un actor y cada liga entre dos actores indica que un actor depende de otro en algo para que
el primero logre cumplir con una meta'' \cite{istarWiki}. Cabe resaltar que el SDM no muestra los detalles que conlleva la red de dependencias
para el logro de los objetivos generales del sistema. Dada la definición anterior, se puede resumir que el SDM es un modelo que muestra la
intencionalidad de los actores del sistema y sus dependencias estratégicas para el logro de objetivos generales. A continuación se muestran algunos
elementos de modelado del marco de trabajo i* para la construcción de un SDM.\\

\newpage
\noindent Los actores del marco de trabajo i* son mostrados en la Figura \ref{istarActores}:

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.75]{Capitulo4/figs/istarActores.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Actores en i* \cite{istarWiki} }            %Pie de imagen
  \label{istarActores}                            %nombre de referencia
\end{figure}

\noindent Los actores presentados en la figura anterior se definen de acuerdo a istarwiki.org \cite{istarWiki} de la siguiente manera:\\

\begin{itemize}
  \item [a)] \textbf{Actores} Son entidades que realizan acciones para lograr una meta. Se utiliza el término actor para hacer referencia a cualquier unidad a la cual
  se le puedan atribuir dependencias intencionales. Los agentes, roles y posiciones son actores especializados.

  \item [b)] \textbf{Agente.} Es un actor con una concreta manifestación física, tal como un ser humano.

  \item [c)]  \textbf{Posición.} Es una abstracción intermedia que puede ser usada entre un rol y un agente.

  \item [d)]  \textbf{Rol.} Es una caracterización abstracta del comportamiento de un actor en un contexto especializado.\\

\end{itemize}



\noindent En la Figura \ref{istarElements} son mostrados los elementos de i*:\\

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.75]{Capitulo4/figs/istarElements.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Elementos de i* \cite{istarWiki} }            %Pie de imagen
  \label{istarElements}                            %nombre de referencia
\end{figure}

\newpage
\noindent Los elementos mostrados en la figura anterior se definen de acuerdo a istarwiki.org \cite{istarWiki} de la siguiente manera:\\

\begin{itemize}

  \item [a)] \textbf{Meta.} Representa la intención de un actor. La especificación de como la meta es satisfecha no es descrita por ella misma.

  \item [b)] \textbf{Tarea.} El actor quiere cumplir alguna tarea específica, realizada de un modo particular. La especificación de la tarea puede ser descrita descomponiendo la tarea
  en sub-elementos.

  \item [c)] \textbf{Recurso.} Provee al actor de una entidad física o de información.

  \item [d)] \textbf{Meta suave.} Son similares a las metas, con la excepción de que el criterio para la satisfacción no es claro y tiende a ser juzgado por el punto de vista del actor.\\

\end{itemize}


\noindent En la Figura \ref{istarDependency} se muestra cómo se modelan las dependencias estrátegicas:\\

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.75]{Capitulo4/figs/istarDependency.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Dependencias estratégicas en i* \cite{istarWiki} }            %Pie de imagen
  \label{istarDependency}                            %nombre de referencia
\end{figure}

Las dependencias estratégicas son modeladas mediante una liga que contiene una especie de ``D''. Las ligas dependencia son leídas en el sentido de la ``D'',
esto quiere decir que, en el caso de la figura mostrada anteriormente el actor ingeniero, depende de el actor empleador para ser contratado.


\section{Refinamiento de los requisitos de alto nivel}
\noindent El segundo paso de la metodología consiste en la reducción del grado de abstracción del modelo estratégico de dependencias, para
ello se elabora un modelo estratégico racional i*, SRM por sus siglas en inglés. Con un SRM a diferencia del SDM, se documenta la estructura lógica interna
de cada actor dentro un límite, representado como una línea circular, tal como es mostrado en la Figura \ref{contextoActor}. El SRM también plasma las relaciones
que existen entre un actor y otro.

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.475]{Capitulo4/figs/contextoActor.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Límites de un actor en i*}            %Pie de imagen
  \label{contextoActor}                            %nombre de referencia
\end{figure}

La estructura lógica interna de los actores es definida en términos de metas, tareas, recursos y metas suaves (atributos de calidad),
los cuales se relacionan por medio de diferentes tipos ligas tal como puede ser observado en la Figura \ref{istarRel}.


\begin{figure}[h]
  \centering
    \includegraphics[scale=0.475]{Capitulo4/figs/istarRelationships.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Relaciones del Framework i*}            %Pie de imagen
  \label{istarRel}                            %nombre de referencia
\end{figure}

\begin{itemize}
  \item [a)] \textbf{Medios para lograr.} Representa que un elemento es un medio para lograr la completitud de otro elemento. En este caso, una tarea es necesaria para el logro de la meta.
  \item [b)] \textbf{Descomposición.} Sirven para representar la descomposición de tareas en diversos elementos.
  \item [c)] \textbf{Contribución.} Indican la existencia de una contribución positiva o negativa para el logro
de los atributos de calidad del sistema
\end{itemize}

Un ejemplo completo de un modelo estratégico de dependencias puede ser observado a continuación en la Figura \ref{srm}.\\

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.75]{Capitulo4/figs/srm.jpg}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Modelo estratégico de dependencias \cite{Yu01}}            %Pie de imagen
  \label{srm}                            %nombre de referencia
\end{figure}


En esta etapa de la metodología, además de la realización del SRM, se procede a la elaboración de diagramas de secuencia
de sistema de las tareas que en este modelo impliquen un proceso de desarrollo. La elaboración de diagramas de secuencia de sistema en esta etapa
tiene la finalidad de establecer el flujo de ejecución de los procesos del sistema en cuestión.

%\newpage







\begin{spacing}{1.15}
\section{Especificación del modelo de información de entidades físicas y virtuales}
\end{spacing}
\noindent El tercer paso de MeDAIC consiste en la definición de un modelo de información de las entidades físicas de las cuales existe interés por
compartir algún tipo de información y su relación con otras entidades virtuales; por ejemplo, en el caso de un sistema de monitoreo para la entrega de productos agrícolas una de las entidades
de interés sería la unidad vial que se encarga de transportar los productos. La elaboración de este mmodelo se realiza utilizando UML.\\


La Figura \ref{modeloInformacion} representa un ejemplo de este tipo de modelo. De la unidad vial se identifican como atributos de interés el identificador de la unidad,
su geolocalización, velocidad y temperatura a la cual se encuentra el vagón donde se almacenan los productos agrícolas. La información de estos atributos
serán los datos que la unidad vial envíe al sistema de monitoreo para que finalmente estos sean enviados a los correspondientes clientes que se encuentren suscritos al servicio. Un cliente puede
ser cualquier dispositivo, desde un sistema complejo hasta un reloj inteligente o incluso un simple dispositivo como un motor que se accione por efecto de
la información que se esté recibiendo por la red.\\

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.665]{Capitulo4/figs/modeloEntidades.png}      %Ruta completa de la imagen, porque se compila desde el archivo tesis.tex
  \caption{Ejemplo de modelo de entidades físicas y virtuales}            %Pie de imagen
  \label{modeloInformacion}                            %nombre de referencia
\end{figure}


\begin{spacing}{1.15}
\section{Definición del formato y estructura para el intercambio de datos}
\end{spacing}
\noindent En este paso se define el formato y estructura con el que será enviada la información de las entidades físicas hacia los
clientes suscritos al sistema. Se puede elegir cualquier formato deseado, pero se recomienda ampliamente el uso de la notación de objetos de
javaScript (JSON) por tener una notación simple y ser capaz de ser utilizado para intercambiar información entre programas escritos en todos los lenguajes de programación modernos.\\

JSON está basado en la notación de objetos literales de JavaScript, la cual es una de sus mejores partes.
A pesar de que este formato es un subconjunto de JavaScript, es independiente del lenguaje. JSON es un formato de texto, así que es leible por humanos
y máquinas. Es fácil de implementar y de utilizar \cite{Crockford2008}.

\section{Visión general de la arquitectura}
\noindent El siguiente paso es el desarrollo de una visión general de la arquitectura del sistema. La visión general de la arquitectura se presenta a los
diferentes stakeholders para brindar un panorama claro sobre la propuesta de construcción del sistema. Esto permitirá dar conocimiento sobre las interfaces
del sistema, cómo se verán satisfechos requisitos de calidad y cómo es que el sistema podría evolucionar en un momento dado.

\section{Construcción, pruebas y validación}
\noindent En este paso se procede al desarrollo y pruebas del sistema; esto incluye el desarrollo de prototipos hardware y la definición del alcance del ciclo de trabajo con los stakeholders. Una vez terminados los procesos de construcción y pruebas se procede a realizar una reunión con los
stakeholders para verificar si efectivamente se está logrando el cumplimiento de los requisitos o si existe algún requisito que necesite ser
modificado o agregado. En caso de modificaciones a los requerimientos se vuelve al paso número dos para realizar las modificaciones pertinentes
a los modelos; en caso de cumplir con las expectativas de los stakeholders se procede al siguiente paso.

\section{Desarrollo del diagrama de despliegue}
\noindent Como penúltimo paso se tiene el desarrollo de un diagrama de despliegue. La elaboración del diagrama de despliegue tiene la finalidad de capturar las relaciones entre un particular elemento conceptual o físico que conforman el sistema modelado y los artefactos software que contienen \cite{Omg2013}. Sirve como guía para llevar a cabo la implantación del sistema en cuestión.



\section{Implantación del sistema}
\noindent Como último paso de la metodología MeDAIC, se procede a realizar la implantación del sistema.
El ingeniero encargado de realizar la implantación deberá tomar el diagrama de despliegue como base de referencia, para que de acuerdo a las
a las especificaciones topológicas plasmadas en este diagrama realice una correcta implantación del sistema.
